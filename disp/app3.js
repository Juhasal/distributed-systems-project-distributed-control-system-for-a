// **** Remote ***

var express = require('express')
	, app = express()
	, server = require('http').createServer(app)
	, path = require('path')
	, io = require('socket.io').listen(server)	//integrate socket.io
	, spawn = require('child_process').spawn	//creates a child process
	, PythonShell = require('python-shell')

	, fs = require('fs')		//file operations
	, amqp = require('amqplib/callback_api')		//amqp library
	//, ioe = require('socket.io-emitter')({host: '102.268.0.100', port: 8081});


//Socket.io config
io.set('log level', 1); //minify log files

//all environments
//jade:
app.set('views', __dirname + '/tpl');
app.set('view engine', "jade");
app.engine('jade', require('jade').__express);

app.set('port',process.env.TEST_PORT || 8082); 	//set port as 8080
app.use(express.favicon());			//set icon
app.use(express.logger('dev'));			//log
app.use(express.bodyParser());			//body parser
app.use(express.methodOverride());		//enables the use of http methods
app.use(express.static(path.join(__dirname, 'public')));//basic static public server

//Routes

app.get('/', function(req, res) {
	res.render("index"); 	// @ /tmp/index
});

app.get('/remote3', function(req, res) {
	res.render("remote3");
});

server.listen(app.get('port'), function() {
	console.log('Express server listening on port ' + app.get('port'));
});

//***LOGGING***
//*initialize log file
fs.writeFile('log_player.txt', ' ', function (err) {
  if (err) throw err;
  console.log('[log] log file created');
});
//***LOGGER-FUNCTION***
function logger(data){
fs.appendFile('log_player.txt',  data+'\n', function (err) {
  if (err) throw err;
  //console.log('[log] %s logged', data);
  });
}
//usage:
logger('Log file for musicbox');
logger('***');



//AMQP
var queue = 'test_queue';
console.log('test1');
//pub
//creates channel and sends buffer to it
function publisher(conn){
	conn.createChannel(on_open);
	function on_open(err, ch){
		if (err != null) bail(err);
		ch.assertQueue(queue);
		ch.sendToQueue(queue, new Buffer('HelloWooorld'));
	}
}

var playlist_json = []

var message_amqp = '';
var key = '';
//sub
//reads queue, converts to string, print
function consumer(conn){
	var ok = conn.createChannel(on_open);
	function on_open(err, ch){
		if (err != null) bail(err);
		var ex = 'remote_control';
		ch.assertExchange(ex, 'fanout', {durable: false});	
		// SUB:
		// assert queue and bind to exchange 
		ch.assertQueue('', {exclusive: true}, function(err, ok){
		var q = ok.queue;
		ch.bindQueue(ok.queue, 'control', '')
		//init.msg
		console.log('init. msg sent');
		ch.publish(ex, '', new Buffer ('initialize_state_display'));

		ch.consume(q, function(msg){
			if (msg != null){
				//message_amqp = msg.content.toString();
				message_amqp = msg.content.toString();
				//console.log(message_amqp)
				console.log(" [message] '%s' '%s'", msg.fields.routingKey, msg.content.toString());
				logger(message_amqp);
				ch.ack(msg);	
			// **NEXT_SONG***

			//track name:
				if (msg.fields.routingKey == 'track_key'){
					console.log('track info received');
					//lähetä viesti clientille
					io.sockets.emit('next_track',message_amqp);
					//emitEvent(message_amqp);
				}
			//artist name
				if (msg.fields.routingKey == 'artist_key'){
					console.log('track info received');
					io.sockets.emit('next_artist',message_amqp);
				}
			//album name
				if (msg.fields.routingKey == 'album_key'){
					console.log('album info received');
					//lähetä viesti clientille
					io.sockets.emit('album',message_amqp);
				}
			//duration
				if (msg.fields.routingKey == 'duration_key'){
					console.log('duration info received');
					//lähetä viesti clientille
					io.sockets.emit('duration',message_amqp);
				}
				if (msg.fields.routingKey == 'playlist_key'){
					console.log('playlist info received');
					//lähetä viesti clientille
					//io.sockets.emit('duration',message_amqp);
					//var playlist_str = message_amqp;
					playlist_json = JSON.parse(message_amqp)
					console.log(playlist_json)
					console.log(playlist_json[2])
					console.log(playlist_json.data)
				}
				if (msg.fields.routingKey == 'tracknumber_key'){
					console.log('track number received');
					//lähetä viesti clientille
					io.sockets.emit('duration',message_amqp);
					var tracknumber_int = Number(message_amqp);
					console.log(tracknumber_int);
					var next_song = playlist_json[tracknumber_int+1]
					console.log('next track: ' + next_song)
					io.sockets.emit('next_next_track',next_song);
				}


				//***NEXT_TRACK***
				if (msg.fields.routingKey == 't_key'){//(message_amqp == 'next_track'){
					//io.on('connection', function (socket){
					console.log('Key: %s ',msg.fields.routingKey);
					console.log('next_track command received');
					//test message
					ch.publish('remote_control', 'testkey', new Buffer('test1'));
					io.sockets.emit('next_track','Track_name1');
					//io.sockets.emit('next_track','Track_name1');
					//emitEvent(message_amqp);
				}


				//***INITIALIZE_STATE***
				if (message_amqp == 'initialize_state'){
					//io.on('connection', function (socket){
					console.log('init. request received');
					//respond to init. request
					ch.publish('remote_control', '', new Buffer ('initialize_state_display'));
					console.log('init: respond')
					logger('[init] connected to server');
				}	
				
			}
	
		});		
		});	
		//INIT msg:
		//liian aikaisin? asynkronisuusongelmia?
		//console.log('init. msg sent');
		//ch.publish(ex, '', new Buffer ('initialize_state_display'));
		
	
//huom! place inside channel definition
	io.on('connection', function (socket){

	console.log("user connected");
	logger('[init] client connected');
	socket.emit('message', {message: 'connected'});

	socket.on("random", function (data){
		socket.type = "remote2";
		console.log("Play random track");
		logger('[client] random_track');
		ch.publish('remote_control', 'random_key', new Buffer('random'));
	});
	
	socket.on("sstop", function (data){
		socket.type = "remote2";
		console.log("Start/Stop");
		ch.publish('remote_control', 'sstop_key', new Buffer('sstop'));
	});

	socket.on("remote", function (data){
		socket.type = "remote2";
		console.log("Remote client ready...");	
			
	});
	socket.on("next", function (data){
		socket.type = "remote2";
		console.log("Play next song");
		logger('[client] next_track');
		ch.publish('remote_control', 'next_key', new Buffer('next_song'));
		
	});


});
	}
}


function emitEvent(data){
		console.log('emit message: '+data);
		io.sockets.emit(data);
}
//connects to virtual host and sets itself as a consumer
console.log('connecting...');
amqp.connect('amqp://test:test@192.168.0.185:5672/vhost_test', function(err, conn){
	
	if (err != null) bail(err);
		console.log('connected to virtual host');
		logger('[init]connected to virtual host');
		consumer(conn);
		
});

function bail(err){
	console.error(err);
}


