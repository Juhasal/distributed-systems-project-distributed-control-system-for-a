#Python_musicplayer
#Playback with pygame.mixer
#Read tags with mutagen

import string
import pygame
import pygame.mixer
from mutagen.easyid3 import EasyID3
import mutagen
pygame.init()
pygame.mixer.init()
pygame.mixer.get_init()

#File:
#musicfile = '/home/pi/music/01_IOU.mp3';
musicfile = '/home/juha/Music/Live It Out/02 Glass Ceiling.mp3';
id3file = EasyID3(musicfile)

#Print Album:
#muuta stringiksi ja poista turhat merkit, 3 ensimmaista ja 2 viimeista.
album_id3 = id3file["album"]
album_str = str(album_id3)
album_length = len(album_str)
album = album_str[3:album_length-2]
print "Album: %s" % album


#Print Title:
title_id3 = id3file["title"]
title_str = str(title_id3)
title_length = len(title_str)
title = title_str[3:title_length-2]
print "Title: %s" % title

#Kaikki tagit:
#print(id3file.pprint())

#Play track:
pygame.mixer.music.load(musicfile)
pygame.mixer.music.play()
#raw_input()
while pygame.mixer.music.get_busy() == True:
    continue

