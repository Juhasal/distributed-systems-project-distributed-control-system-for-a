#!/usr/bin/python

from Adafruit_CharLCD import Adafruit_CharLCD
import sys
import time

lcd = Adafruit_CharLCD()

lcd.begin(16, 1)

title = str(sys.argv[1])
artist = str(sys.argv[2])

print title
print artist


lcd.clear()

lcd.message("%s \n" % (title))
lcd.message("%s" % (artist))
#time.sleep(1)
