***README***

Distributed systems / Hajautetut järjestelmät 

Coursework project.

*Summary:*

This project implements a music player as a web-based distributed system with three nodes that communicate with each other. 

*Application overview:*

The application designed in this course work is a music player that uses web-based interface to control the player and display information remotely. The music player works on one node, the remote controller on one node, and the display on one node. Playlists are generated from the mp3-files in a directory and shared between nodes. The track information is parsed from id3-tags in mp3-files and also shared to remote nodes. The application uses node.js and express framework in local web server, and HTML and Jade template engine in the client-side functionality. CSS stylesheets are also used in user interface. Communication between the servers is done with RabbitMQ, which uses AMQP (Advanced Message Queuing Protocol) in messaging. There's also other technologies used in the music player node, for example Python for constructing playlists in json format, and groovelib-library for playing the music files.

More in-depth report of the project is included in the sources.